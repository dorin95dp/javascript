/**
 * Created by Popa on 24.11.2016.
 */

// MARK: Global variables

var xSquare = /x\^2/; // x square regular expression
var justX = /x[^\^2]/; // x without x square regular expression

var expression = prompt("Please enter the equation");
processExpression(expression);

// MARK: Global functions

function insertString(item, string, at) {
    var position = string.search(at);
    
    if (position != -1) {
        return string.substr(0, position) + item + string.substr(position);
    }

    return string;
}

function getRoots(a, b, c){
    var x1, x2;
    
    if (a == 0) {
        return -c/b;
    }

    var delta = Math.pow(b,2) - 4*a*c;
    
    if (delta < 0) {
        return null;
    } else if (delta == 0) {
        return -b/(2*a);
    } else {
        x1 = (-b - Math.sqrt(delta)) / (2*a);
        x2 = (-b + Math.sqrt(delta)) / (2*a);

        return [x1, x2];
    }
}

function hasAllCoefficients(array){
    return array.length == 3;
}

function hasTwoCoefficients(array){
    return array.length == 2;
}

function hasXSquare(expression) {
    return /x\^2/.test(expression);
}

function hasX(expression) {
    return /x[^\^2]/.test(expression);
}

function doesNotHaveFirstCoefficient(expression) {
    if (hasXSquare(expression)) {
        var match = xSquare.exec(expression);
        if (!/\d/.test(expression[match.index - 1])) {
            return true;
        }
    }

    return false;
}

function doesNotHaveSecondCoefficient(expression) {
    if (hasX(expression)) {
        var match = justX.exec(expression);
        if (!/\d/.test(expression[match.index - 1])) {
            return true;
        }
    }

    return false;
}

function insertMissingCoefficientsIn(expression) {
    if (doesNotHaveFirstCoefficient(expression)) {
        expression = insertString('1', expression, xSquare);
    }
    if (doesNotHaveSecondCoefficient(expression)){
        expression = insertString('1', expression, justX);
    }

    return expression;
}

function transformToTokens(expression) {
    expression = insertMissingCoefficientsIn(expression);
    return expression.replace(/(=|0|\^2)/igm, '').split('x');
}

function getCoefficients(expression){
    var tokens = transformToTokens(expression);
    var a = b = c = 0;

    if (tokens.length === 3) {
        return tokens;
    }

    var xCount = expression.split('x').length - 1;

    if (xCount === 2) {
        [a, b] = tokens;
    } else if (xCount === 1) {
        if (hasXSquare(expression)) {
            [a, c] = tokens;
        } else {
            [b, c] = tokens;
        }
    } else {
        [c] = tokens;
    }

    return [a, b, c];
}

function isValid(expression){
    var re = /(\s*)(\d*x\^2(\s*)(\+|-))?(\s*)(\d*x(\s*)(\+|-))?(\s*)(\d*)?(\s*)=(\s*)0(\s*)/;
    var reLeastAnArgument = /x/;

    return re.test(expression) && reLeastAnArgument.test(expression);
}

function processExpression(expression){
    if (isValid(expression)) {
        var coefficients = getCoefficients(expression);
        var a = coefficients[0];
        var b = coefficients[1];
        var c = coefficients[2];
        var roots = getRoots(a, b, c);
        alert("The roots are: " + roots);
    } else {
        alert("The equation is not valid! Please rewrite it correctly or try another one.");
    }
}
